const myForm = document.querySelector("#my-form");
const passwordInput = document.querySelector("#password");
const emailInput = document.querySelector("#email");
const genderInput = document.querySelector("#gender");
const authorOption = document.getElementsByName("author");
const inputElements = document.getElementsByClassName("messageCheckbox");

const msg = document.querySelector(".msg");
const userList = document.querySelector("#users");

myForm.addEventListener("submit", onSubmit);

function onSubmit(e) {
  e.preventDefault();
  let gendIn = genderInput.options[genderInput.selectedIndex].value;
  let authorChecked;
  for (i = 0; i < authorOption.length; i++) {
    if (authorOption[i].checked) {
      authorChecked = authorOption[i].value;
    }
  }

  let checkedValue = [];
  for (var i = 0; inputElements[i]; ++i) {
    if (inputElements[i].checked) {
      checkedValue.push(inputElements[i].value);
    }
  }
  console.log(checkedValue, checkedValue.length);

  if (
    passwordInput.value === "" ||
    emailInput.value === "" ||
    gendIn === "" ||
    authorChecked === undefined ||
    checkedValue.length < 2
  ) {
    msg.innerHTML = "Please enter all fields";

    // Remove error after 3 seconds
    setTimeout(() => msg.remove(), 3000);
  } else {
    // Create new list item with user
    const li = document.createElement("li");

    // Add text node with input values
    li.appendChild(
      document.createTextNode(
        `${passwordInput.value}: ${emailInput.value} : ${gendIn} : ${authorChecked} :`
      )
    );

    // Append to ul
    userList.appendChild(li);

    // Clear fields
    passwordInput.value = "";
    emailInput.value = "";
  }
}
